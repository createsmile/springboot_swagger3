package com.tencentbridge.swagger3;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "接口展示")
public class TestController {
    @ApiOperation("查询测试")
    @GetMapping("test")
    public String test() {
        return "hello world";
    }
}
